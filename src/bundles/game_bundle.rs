use amethyst::{
    core::{SystemBundle},
    ecs::{
        prelude::DispatcherBuilder,
        World
    },
    Error
};
use crate::systems::{
    GeneratePlanetSystem,
    RotatePlanetSystem,
    FollowCameraSystem,
};

pub struct GameBundle;

impl GameBundle {
    pub fn new() -> Self {
        GameBundle
    }
}

impl<'a, 'b> SystemBundle<'a, 'b> for GameBundle {
    fn build(self, _world: &mut World, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<(), Error> {
        // builder.add(MovePlanetsSystem, "move_planets", &[]);
        // builder.add(DestroyPlanetSystem, "destroy_planets", &["move_planets"]);
        builder.add(RotatePlanetSystem, "rotate_planets", &[]);
        builder.add(GeneratePlanetSystem, "generate_planets", &[]);
        builder.add(FollowCameraSystem, "follow_camera", &[]);

        Ok(())
    }
}