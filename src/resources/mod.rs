pub use self::galaxy::GalaxyResource;
pub use self::camera::GameCamera;

mod galaxy;
mod camera;