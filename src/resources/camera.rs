use amethyst::ecs::Entity;

#[derive(Default)]
pub struct GameCamera(pub Option<Entity>);