use amethyst::{
    assets::Handle,
    renderer::SpriteSheet
};

pub struct GalaxyResource {
    pub planet_count: u32,
    pub planet_capacity: u32,
    pub planet_asset_handle: Option<Handle<SpriteSheet>>,
    pub origin: (f32, f32)
}

impl Default for GalaxyResource {
    fn default() -> Self {
        GalaxyResource {
            planet_count: 0,
            planet_capacity: 50,
            planet_asset_handle: None,
            origin: (1920.0 / 2.0, 1080.0 / 2.0)
        }
    }
}