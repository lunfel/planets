use amethyst::{
    prelude::*,
    assets::{AssetStorage, Loader, Handle},
    core::transform::Transform,
    ecs::{Write},
    renderer::{
        Camera,
        ImageFormat,
        Texture,
        SpriteSheetFormat,
        SpriteSheet,
        SpriteRender,
    },
    input::{is_close_requested, is_key_down, VirtualKeyCode}
};
use crate::resources::GalaxyResource;
use crate::resources::GameCamera;

#[derive(Default)]
pub struct SolarSystem;

impl SimpleState for SolarSystem {
    fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> SimpleTrans {
        if let StateEvent::Window(event) = &event {
            if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
                return Trans::Quit;
            }
        }
        
        Trans::None
    }

    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        let sprite_sheet_handle = load_sprite_sheet(world);
        let background_asset_handle = load_background(world);
        initialize_camera(world);

        let mut background_transform = Transform::default();
        background_transform.set_translation_xyz(1920.0 / 2.0, 1080.0 / 2.0, -1.0);
        let sprite_render = SpriteRender::new(background_asset_handle.clone(), 0);

        world.register::<Transform>();
        world.register::<SpriteRender>();

        world.create_entity()
            .with(background_transform)
            .with(sprite_render)
            .build();

        world.exec(|mut galaxy: Write<GalaxyResource>| {
            galaxy.planet_asset_handle.replace(sprite_sheet_handle.clone());
        });
    }
}

fn load_sprite_sheet(world: &mut World) -> Handle<SpriteSheet> {
    let loader = world.read_resource::<Loader>();

    let texture_handle = {
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "textures/planets.png",
            ImageFormat::default(),
            (),
            &texture_storage
        )
    };
    
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "spritesheets/planets.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store
    )
}

fn load_background(world: &mut World) -> Handle<SpriteSheet> {
    let loader = world.read_resource::<Loader>();

    let texture_handle = {
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "textures/starlight-background.png",
            ImageFormat::default(),
            (),
            &texture_storage
        )
    };
    
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "spritesheets/background.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store
    )
}

fn initialize_camera(world: &mut World)
{
    let mut camera_transform = Transform::default();
    camera_transform.set_translation_xyz(1920.0 * 0.5, 1080.0 * 0.5, 1.0);

    let entity = world.create_entity()
        .with(Camera::standard_2d(1920.0, 1080.0))
        .with(camera_transform)
        .build();

    world.exec(|mut camera: Write<GameCamera>| {
        camera.0.replace(entity);
    })
}