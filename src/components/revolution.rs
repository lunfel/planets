use amethyst::ecs::{Component, DenseVecStorage};

#[derive(Debug)]
pub struct RevolutionComponent {
    pub rad_per_sec: f32,
    pub distance_origin: (f32, f32),
    pub rotation: f32,
}

impl Component for RevolutionComponent {
    type Storage = DenseVecStorage<Self>;
}

impl Default for RevolutionComponent {
    fn default() -> Self {
        RevolutionComponent {
            rad_per_sec: 1.0,
            distance_origin: (50.0, 50.0),
            rotation: 0.0
        }
    }
}