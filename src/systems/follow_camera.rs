use amethyst::{
    core::transform::Transform,
    ecs::{System, Read, ReadStorage, WriteStorage, Join},
    input::{InputHandler, StringBindings},
    renderer::Camera
};

pub struct FollowCameraSystem;

impl<'s> System<'s> for FollowCameraSystem {
    type SystemData = (
        Read<'s, InputHandler<StringBindings>>,
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Camera>
    );

    fn run(&mut self, (input, mut transforms, cameras): Self::SystemData) {
        if let Some((x, y)) = input.mouse_position() {
            for (transform, _) in (&mut transforms, &cameras).join() {
                transform.set_translation_xyz(x * 1.0, (1080.0 - y) * 1.3, 1.0);
            }
        }
    }
}