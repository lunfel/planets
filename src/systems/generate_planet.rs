use amethyst::{
    core::{Transform},
    renderer::{SpriteRender},
    ecs::{System, WriteStorage, Entities, Write}
};

use crate::components::revolution::RevolutionComponent;
use crate::resources::GalaxyResource;
use rand::prelude::*;

pub struct GeneratePlanetSystem;

impl<'s> System<'s> for GeneratePlanetSystem {
    type SystemData = (
        Write<'s, GalaxyResource>,
        WriteStorage<'s, RevolutionComponent>,
        WriteStorage<'s, SpriteRender>,
        WriteStorage<'s, Transform>,
        Entities<'s>
    );

    fn run(&mut self, (mut galaxy, mut revolutions, mut sprites, mut transforms, entities): Self::SystemData) {
        let missing_planets = galaxy.planet_capacity - galaxy.planet_count;
        let mut rng = rand::thread_rng();
    
        if let Some(sprite_sheet_handle) = galaxy.planet_asset_handle.clone() {
            galaxy.planet_count = galaxy.planet_capacity;
            for _ in 0..missing_planets {
                let sprite_render = SpriteRender::new(sprite_sheet_handle.clone(), rng.gen_range(0..4));
                let mut planet_transform = Transform::default();
                let revolution = RevolutionComponent {
                    distance_origin: (rng.gen_range(50.0..1500.0), rng.gen_range(50.0..1500.0)),
                    rotation: 0.0,
                    rad_per_sec: rng.gen_range(0.05..0.5)
                };

                let origin_x = 1920.0 / 2.0;
                let origin_y = 1080.0 / 2.0;

                planet_transform.set_translation_xyz(origin_x + revolution.distance_origin.0, origin_y, 0.5);
                
                entities.build_entity()
                    .with(sprite_render.clone(), &mut sprites)
                    .with(revolution, &mut revolutions)
                    .with(planet_transform, &mut transforms)
                    .build();
            }
        }
    }
}
