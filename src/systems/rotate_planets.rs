use amethyst::{
    core::{
        Transform,
        timing::Time
    },
    ecs::{Join, Read, WriteStorage, System}
};
use crate::components::revolution::RevolutionComponent;
use crate::resources::GalaxyResource;

pub struct RotatePlanetSystem;

impl<'s> System<'s> for RotatePlanetSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        WriteStorage<'s, RevolutionComponent>,
        Read<'s, GalaxyResource>,
        Read<'s, Time>
    );

    fn run(&mut self, (mut transforms, mut revolutions, galaxy, time): Self::SystemData) {
        for (transform, revolution) in (&mut transforms, &mut revolutions).join() {
            revolution.rotation += revolution.rad_per_sec * time.delta_seconds();

            let trans_x = galaxy.origin.0 + (revolution.distance_origin.0 * revolution.rotation.cos());
            let trans_y = galaxy.origin.1 + (revolution.distance_origin.1 * revolution.rotation.sin());

            transform.set_translation_xyz(trans_x, trans_y, 0.0);
        }
    }
}