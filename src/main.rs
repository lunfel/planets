mod bundles;
mod components;
mod resources;
mod states;
mod systems;

use amethyst::{
    prelude::*,
    utils::application_root_dir,
    input::{InputBundle, StringBindings},
    core::TransformBundle,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    }
};
use crate::states::SolarSystem;
use crate::bundles::GameBundle;
use std::path::PathBuf;

// small change

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;
    let game_data = initialize_game_data(&app_root)?;
    let assets_dir = app_root.join("assets");

    let mut game = Application::build(assets_dir, SolarSystem::default())?
        .build(game_data)?;
    
    game.run();

    Ok(())
}

fn initialize_game_data<'a, 'b>(app_root: &PathBuf) -> amethyst::Result<GameDataBuilder<'a, 'b>> {
    GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(InputBundle::<StringBindings>::new())?
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(app_root.join("config").join("display.ron"))?
                        .with_clear([0.0, 0.0, 0.0, 1.0]),
                )
                .with_plugin(RenderFlat2D::default())
        )?
        .with_bundle(GameBundle::new())
}